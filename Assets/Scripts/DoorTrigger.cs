﻿using UnityEngine;
using System.Collections;

public class DoorTrigger : MonoBehaviour {

	public Door door;
	public bool ignoreTrigger;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D target) {
		if (ignoreTrigger) {
			return;
		}
		if (target.gameObject.tag == "Player") {
			door.Open();
		}
	}

	void OnTriggerExit2D(Collider2D target) {
		if (ignoreTrigger) {
			return;
		}
		if (target.gameObject.tag == "Player") {
			door.Close();
		}
	}

	public void Toggle(bool value) {
		if (value) {
			door.Open();
		} else {
			door.Close();
		}
	}

	void OnDrawGizmos() {
		Gizmos.color = ignoreTrigger ? Color.gray : Color.green;
		var boxCollider2d = GetComponent<BoxCollider2D>();
		var boxCollider2dPosition = boxCollider2d.transform.position;
		var newPos = new Vector2(boxCollider2dPosition.x + boxCollider2d.center.x, boxCollider2dPosition.y + boxCollider2d.center.y);
		Gizmos.DrawWireCube (newPos, new Vector2(boxCollider2d.size.x, boxCollider2d.size.y));
	}
}
