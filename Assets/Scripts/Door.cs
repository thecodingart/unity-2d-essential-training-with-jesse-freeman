﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public enum State{Idle = 0, Opening, Open, Closing};

	private State state = State.Idle;
	private Animator animator;
	public float closeDelay = .5f;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnOpenStart() {
		state = State.Opening;
	}

	void OnOpenEnd() {
		state = State.Open;
	}

	void OnCloseStart() {
		state = State.Closing;
	}

	void OnCloseEnd() {
		state = State.Idle;
	}

	void DisableCollider2D() {
		collider2D.enabled = false;
	}

	void EnableCollider2D() {
		collider2D.enabled = true;
	}

	public void Open() {
		animator.SetInteger("AnimState", 1);
	}

	public void Close() {
		StartCoroutine(CloseNow ());
	}

	private IEnumerator CloseNow() {
		yield return new WaitForSeconds(closeDelay);
		animator.SetInteger("AnimState", 2);
	}
}
